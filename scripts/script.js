var buttons = document.getElementsByTagName('span');
var result = document.querySelectorAll('.result p')[0];
var clear = document.getElementsByClassName('clear')[0];


clear.addEventListener('click', clearResult());

function addValue(i) {
    return function() {
        if (buttons[i].innerHTML === "÷") {
            result.innerHTML += "/";
        } else if (buttons[i].innerHTML === "x") {
            result.innerHTML += "*";
        } else {
            result.innerHTML += buttons[i].innerHTML;
        }
    };
}

function calculate() {
    return function() {
        result.innerHTML = eval(result.innerHTML);
    }
}

function clearResult() {
    return function(){
        result.innerHTML = "";
    }
}