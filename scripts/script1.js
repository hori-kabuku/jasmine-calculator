var buttons = document.getElementsByTagName('span');
var display = document.querySelectorAll('.display p')[0];
var clear = document.getElementsByClassName('clear')[0];

for (var i = 0; i < buttons.length; i++) {
    if (buttons[i].innerHTML === "=") {
        buttons[i].addEventListener('click', showResult());
    } else {
        buttons[i].addEventListener('click', addValue(i));
    }
}

clear.addEventListener('click', clearValues());

var firstNumber;
var operator;
var secondNumber;
var result;

function addValue(i) {
    return function() {
        var iHTML = buttons[i].innerHTML;
        console.log("iHTML: " + iHTML);
        var numberInnerHTML = Number(iHTML);
        console.log("numberInnerHTML: " + numberInnerHTML);
        if (isNaN(numberInnerHTML)) {
            assignOperator(iHTML);
            console.log("operator: " + operator);
        } else if (firstNumber && operator) {
            console.log("one");
            secondNumber = numberInnerHTML;
            display.innerHTML = secondNumber;
        } else if (!isNaN(numberInnerHTML)) {
            console.log("zen");
            firstNumber = numberInnerHTML;
            display.innerHTML = firstNumber;
        }
    }
}

function assignOperator(t) {
    return function() {
        console.log("the assignOperator is called");
        if (t === "+") {
            operator = "addition";
        } else if (t === "x") {
            operator = "multiple";
        } else if (t === "÷") {
            operator = "division";
        } else {
            operator = "subtract";
        }
    }();
}

function calculateResult() {
    return function() {
        var r;
        console.log("calculateResult is called");
        if (operator === "addition") {
            console.log("addition");
            r = firstNumber + secondNumber;
        } else if (operator === "multiple") {
            r = firstNumber * secondNumber;
        } else if (operator === "division") {
            r = firstNumber / secondNumber;
        } else if (operator === "subtract") {
            r = firstNumber - secondNumber;
        }
        console.log("r: " + r);
        result = r;
        console.log("result: " + result);
    }();
}


function showResult() {
    return function() {
        console.log("the showResult function is called");
        calculateResult();
        console.log(result);
        display.innerHTML = result;
    }
}

function clearValues() {
    return function() {
        console.log("clearValues is called");
        firstNumber = "";
        operator = "";
        secondNumber = "";
        result = "";
        display.innerHTML = "";
    }
}